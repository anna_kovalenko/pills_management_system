package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pacientdiagnosis.class)
public abstract class Pacientdiagnosis_ {

	public static volatile SingularAttribute<Pacientdiagnosis, Pacients> pacientInn;
	public static volatile SingularAttribute<Pacientdiagnosis, BigDecimal> id;
	public static volatile SingularAttribute<Pacientdiagnosis, Disease> diseaseId;

}

