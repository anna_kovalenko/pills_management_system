package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Recipe.class)
public abstract class Recipe_ {

	public static volatile SingularAttribute<Recipe, Visit> visitId;
	public static volatile SingularAttribute<Recipe, BigDecimal> id;
	public static volatile SingularAttribute<Recipe, Pills> pillsId;

}

