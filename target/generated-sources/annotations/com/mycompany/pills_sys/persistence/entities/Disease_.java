package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Disease.class)
public abstract class Disease_ {

	public static volatile SingularAttribute<Disease, String> symptoms;
	public static volatile SingularAttribute<Disease, String> name;
	public static volatile SingularAttribute<Disease, BigDecimal> id;
	public static volatile CollectionAttribute<Disease, Pacientdiagnosis> pacientdiagnosisCollection;

}

