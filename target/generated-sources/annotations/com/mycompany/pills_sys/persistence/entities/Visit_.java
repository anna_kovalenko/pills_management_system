package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Visit.class)
public abstract class Visit_ {

	public static volatile SingularAttribute<Visit, Doctors> doctorInn;
	public static volatile SingularAttribute<Visit, String> note;
	public static volatile SingularAttribute<Visit, String> diagnosis;
	public static volatile SingularAttribute<Visit, Pacients> pacientInn;
	public static volatile SingularAttribute<Visit, BigDecimal> id;
	public static volatile SingularAttribute<Visit, Date> dateOfVisit;
	public static volatile CollectionAttribute<Visit, Recipe> recipeCollection;

}

