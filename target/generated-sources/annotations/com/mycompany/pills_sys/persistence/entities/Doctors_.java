package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Doctors.class)
public abstract class Doctors_ {

	public static volatile CollectionAttribute<Doctors, Visit> visitCollection;
	public static volatile SingularAttribute<Doctors, String> snp;
	public static volatile SingularAttribute<Doctors, BigDecimal> inn;
	public static volatile SingularAttribute<Doctors, String> job;

}

