package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pills.class)
public abstract class Pills_ {

	public static volatile SingularAttribute<Pills, String> modeOfApp;
	public static volatile SingularAttribute<Pills, String> contraindications;
	public static volatile SingularAttribute<Pills, String> assignment;
	public static volatile SingularAttribute<Pills, String> name;
	public static volatile SingularAttribute<Pills, BigDecimal> id;
	public static volatile SingularAttribute<Pills, String> structure;
	public static volatile SingularAttribute<Pills, BigInteger> age;
	public static volatile CollectionAttribute<Pills, Recipe> recipeCollection;

}

