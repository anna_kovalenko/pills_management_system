package com.mycompany.pills_sys.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pacients.class)
public abstract class Pacients_ {

	public static volatile SingularAttribute<Pacients, Date> birthday;
	public static volatile CollectionAttribute<Pacients, Visit> visitCollection;
	public static volatile SingularAttribute<Pacients, String> snp;
	public static volatile SingularAttribute<Pacients, String> phone;
	public static volatile SingularAttribute<Pacients, BigDecimal> inn;
	public static volatile CollectionAttribute<Pacients, Pacientdiagnosis> pacientdiagnosisCollection;

}

