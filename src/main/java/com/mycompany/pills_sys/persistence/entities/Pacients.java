/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Анна
 */
@Entity
@Table(name = "PACIENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pacients.findAll", query = "SELECT p FROM Pacients p"),
    @NamedQuery(name = "Pacients.findByInn", query = "SELECT p FROM Pacients p WHERE p.inn = :inn"),
    @NamedQuery(name = "Pacients.findBySnp", query = "SELECT p FROM Pacients p WHERE p.snp = :snp"),
    @NamedQuery(name = "Pacients.findByBirthday", query = "SELECT p FROM Pacients p WHERE p.birthday = :birthday"),
    @NamedQuery(name = "Pacients.findByPhone", query = "SELECT p FROM Pacients p WHERE p.phone = :phone")})
public class Pacients implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "INN")
    private BigDecimal inn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "SNP")
    private String snp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthday;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "PHONE")
    private String phone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacientInn")
    private Collection<Visit> visitCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacientInn")
    private Collection<Pacientdiagnosis> pacientdiagnosisCollection;

    public Pacients() {
    }

    public Pacients(BigDecimal inn) {
        this.inn = inn;
    }

    public Pacients(BigDecimal inn, String snp, Date birthday, String phone) {
        this.inn = inn;
        this.snp = snp;
        this.birthday = birthday;
        this.phone = phone;
    }

    public BigDecimal getInn() {
        return inn;
    }

    public void setInn(BigDecimal inn) {
        this.inn = inn;
    }

    public String getSnp() {
        return snp;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @XmlTransient
    public Collection<Visit> getVisitCollection() {
        return visitCollection;
    }

    public void setVisitCollection(Collection<Visit> visitCollection) {
        this.visitCollection = visitCollection;
    }

    @XmlTransient
    public Collection<Pacientdiagnosis> getPacientdiagnosisCollection() {
        return pacientdiagnosisCollection;
    }

    public void setPacientdiagnosisCollection(Collection<Pacientdiagnosis> pacientdiagnosisCollection) {
        this.pacientdiagnosisCollection = pacientdiagnosisCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inn != null ? inn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacients)) {
            return false;
        }
        Pacients other = (Pacients) object;
        if ((this.inn == null && other.inn != null) || (this.inn != null && !this.inn.equals(other.inn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.pills_sys.persistence.entities.Pacients[ inn=" + inn + " ]";
    }
    
}
