/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Анна
 */
@Entity
@Table(name = "VISIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Visit.findAll", query = "SELECT v FROM Visit v"),
    @NamedQuery(name = "Visit.findById", query = "SELECT v FROM Visit v WHERE v.id = :id"),
    @NamedQuery(name = "Visit.findByDateOfVisit", query = "SELECT v FROM Visit v WHERE v.dateOfVisit = :dateOfVisit"),
    @NamedQuery(name = "Visit.findByNote", query = "SELECT v FROM Visit v WHERE v.note = :note"),
    @NamedQuery(name = "Visit.findByDiagnosis", query = "SELECT v FROM Visit v WHERE v.diagnosis = :diagnosis")})
public class Visit implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_OF_VISIT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfVisit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "NOTE")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DIAGNOSIS")
    private String diagnosis;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "visitId")
    private Collection<Recipe> recipeCollection;
    @JoinColumn(name = "PACIENT_INN", referencedColumnName = "INN")
    @ManyToOne(optional = false)
    private Pacients pacientInn;
    @JoinColumn(name = "DOCTOR_INN", referencedColumnName = "INN")
    @ManyToOne(optional = false)
    private Doctors doctorInn;

    public Visit() {
    }

    public Visit(BigDecimal id) {
        this.id = id;
    }

    public Visit(BigDecimal id, Date dateOfVisit, String note, String diagnosis) {
        this.id = id;
        this.dateOfVisit = dateOfVisit;
        this.note = note;
        this.diagnosis = diagnosis;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @XmlTransient
    public Collection<Recipe> getRecipeCollection() {
        return recipeCollection;
    }

    public void setRecipeCollection(Collection<Recipe> recipeCollection) {
        this.recipeCollection = recipeCollection;
    }

    public Pacients getPacientInn() {
        return pacientInn;
    }

    public void setPacientInn(Pacients pacientInn) {
        this.pacientInn = pacientInn;
    }

    public Doctors getDoctorInn() {
        return doctorInn;
    }

    public void setDoctorInn(Doctors doctorInn) {
        this.doctorInn = doctorInn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visit)) {
            return false;
        }
        Visit other = (Visit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.pills_sys.persistence.entities.Visit[ id=" + id + " ]";
    }
    
}
