/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Анна
 */
@Entity
@Table(name = "DOCTORS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Doctors.findAll", query = "SELECT d FROM Doctors d"),
    @NamedQuery(name = "Doctors.findByInn", query = "SELECT d FROM Doctors d WHERE d.inn = :inn"),
    @NamedQuery(name = "Doctors.findBySnp", query = "SELECT d FROM Doctors d WHERE d.snp = :snp"),
    @NamedQuery(name = "Doctors.findByJob", query = "SELECT d FROM Doctors d WHERE d.job = :job")})
public class Doctors implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "INN")
    private BigDecimal inn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "SNP")
    private String snp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "JOB")
    private String job;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctorInn")
    private Collection<Visit> visitCollection;

    public Doctors() {
    }

    public Doctors(BigDecimal inn) {
        this.inn = inn;
    }

    public Doctors(BigDecimal inn, String snp, String job) {
        this.inn = inn;
        this.snp = snp;
        this.job = job;
    }

    public BigDecimal getInn() {
        return inn;
    }

    public void setInn(BigDecimal inn) {
        this.inn = inn;
    }

    public String getSnp() {
        return snp;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @XmlTransient
    public Collection<Visit> getVisitCollection() {
        return visitCollection;
    }

    public void setVisitCollection(Collection<Visit> visitCollection) {
        this.visitCollection = visitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inn != null ? inn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doctors)) {
            return false;
        }
        Doctors other = (Doctors) object;
        if ((this.inn == null && other.inn != null) || (this.inn != null && !this.inn.equals(other.inn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.pills_sys.persistence.entities.Doctors[ inn=" + inn + " ]";
    }
    
}
