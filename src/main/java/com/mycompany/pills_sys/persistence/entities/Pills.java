/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Анна
 */
@Entity
@Table(name = "PILLS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pills.findAll", query = "SELECT p FROM Pills p"),
    @NamedQuery(name = "Pills.findById", query = "SELECT p FROM Pills p WHERE p.id = :id"),
    @NamedQuery(name = "Pills.findByName", query = "SELECT p FROM Pills p WHERE p.name = :name"),
    @NamedQuery(name = "Pills.findByAssignment", query = "SELECT p FROM Pills p WHERE p.assignment = :assignment"),
    @NamedQuery(name = "Pills.findByContraindications", query = "SELECT p FROM Pills p WHERE p.contraindications = :contraindications"),
    @NamedQuery(name = "Pills.findByStructure", query = "SELECT p FROM Pills p WHERE p.structure = :structure"),
    @NamedQuery(name = "Pills.findByModeOfApp", query = "SELECT p FROM Pills p WHERE p.modeOfApp = :modeOfApp"),
    @NamedQuery(name = "Pills.findByAge", query = "SELECT p FROM Pills p WHERE p.age = :age")})
public class Pills implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ASSIGNMENT")
    private String assignment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "CONTRAINDICATIONS")
    private String contraindications;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "STRUCTURE")
    private String structure;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "MODE_OF_APP")
    private String modeOfApp;
    @Column(name = "AGE")
    private BigInteger age;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pillsId")
    private Collection<Recipe> recipeCollection;

    public Pills() {
    }

    public Pills(BigDecimal id) {
        this.id = id;
    }

    public Pills(BigDecimal id, String name, String assignment, String contraindications, String structure, String modeOfApp) {
        this.id = id;
        this.name = name;
        this.assignment = assignment;
        this.contraindications = contraindications;
        this.structure = structure;
        this.modeOfApp = modeOfApp;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getContraindications() {
        return contraindications;
    }

    public void setContraindications(String contraindications) {
        this.contraindications = contraindications;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public String getModeOfApp() {
        return modeOfApp;
    }

    public void setModeOfApp(String modeOfApp) {
        this.modeOfApp = modeOfApp;
    }

    public BigInteger getAge() {
        return age;
    }

    public void setAge(BigInteger age) {
        this.age = age;
    }

    @XmlTransient
    public Collection<Recipe> getRecipeCollection() {
        return recipeCollection;
    }

    public void setRecipeCollection(Collection<Recipe> recipeCollection) {
        this.recipeCollection = recipeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pills)) {
            return false;
        }
        Pills other = (Pills) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.pills_sys.persistence.entities.Pills[ id=" + id + " ]";
    }
    
}
