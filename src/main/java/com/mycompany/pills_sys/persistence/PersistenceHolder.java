/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.persistence;

import java.util.Locale;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Kateryna
 */
public class PersistenceHolder {
    private static EntityManagerFactory emf;
    
    public static EntityManagerFactory getEntityManagerFactory(){
        if(emf == null){
            Locale.setDefault(new Locale ("en", "US"));
            emf = Persistence.createEntityManagerFactory("pills_sysPersistence");  
        }
        return emf;
    }    

    public static void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
        }
    }
}
