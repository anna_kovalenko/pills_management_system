/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Oleksij
 */
public class SecurityWeApplicationInitializer
extends AbstractSecurityWebApplicationInitializer {

    public SecurityWeApplicationInitializer() {
        super(SecurityConfig.class);
    }
}
