/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.controllers;

import javax.faces.bean.ManagedBean;

/**
 *
 * @author Kateryna
 */
@ManagedBean
public class NavigationController {
    private final String homePageUrl = "home";
    private final String doctorsPageUrl = "doctors";
    private final String pillsPageUrl = "pills";
    private final String pacientsPageUrl = "pacients";
    private final String recipePageUrl = "recipe";
    private final String visitPageUrl = "visit";
    
    public String getvisitPageUrl(){
        return visitPageUrl;
    }
    
    public String getrecipePageUrl(){
        return recipePageUrl;
    }
    
    public String getpacientsPageUrl(){
        return pacientsPageUrl;
    }
    
    public String getpillsPageUrl(){
        return pillsPageUrl;
    }
    
    public String getDoctorsPageUrl(){
        return doctorsPageUrl;
    }
    
    public String getHomePageUrl(){
        return homePageUrl;
    }
    
}
