package com.mycompany.pills_sys.controllers;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import java.io.Serializable;

//@SessionScoped
@ManagedBean
public class LoginBean implements Serializable {

    private String userName = null;
    private String password = null;
    private String Name = null;
    private String Nick = null;
    private String Town = null;
    private String Country = null;
    private String Sex = null;
    private String Birthday = null;
    private String AboutYourself = null;

    // @ManagedProperty(value = "#{authenticationManager}")
    protected AuthenticationManager authenticationManager;

    /*public LoginBean() {
        authenticationManager = new CustomAuthenticationManager();
    }*/

    public String login() {
        try {Authentication request = new UsernamePasswordAuthenticationToken(this.getUserName(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getFlash().setKeepMessages(true);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Authorization failed!"));
            return "index?faces-redirect=true";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "You've logged in successfully!"));
        return "index?faces-redirect=true";
    }
    public String logout(){
        SecurityContextHolder.clearContext();
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);

        return "index?faces-redirect=true";
    }

    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() { return Name; }

    public void setName(String Name) { this.Name = Name; }

    public String getNick() { return Nick; }

    public void setNick(String Nick) { this.Nick = Nick; }

    public String getTown() { return Town; }

    public void setTown(String Town) { this.Town = Town; }

    public String getAboutYourself() {
        return AboutYourself;
    }

    public void setAboutYourself(String aboutYourself) {
        AboutYourself = aboutYourself;
    }

    public String getBirthday() {

        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getSex() {

        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }
}