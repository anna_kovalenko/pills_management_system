/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pills_sys.controllers;

/**
 *
 * @author Kateryna
 */
import com.mycompany.pills_sys.persistence.PersistenceHolder;
import com.mycompany.pills_sys.persistence.entities.Doctors;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

@ManagedBean(name = "doctorsController", eager = true)
public class DoctorsController {

    EntityManagerFactory emf;
    Doctors currentDoctor;
    List<Doctors> doctors;

    public DoctorsController() {
        System.out.println("doctorsController started!");
    }

    public String getHeader() {
        return "Some details about us";
    }

    public Doctors getCurrentSpell() {
        return currentDoctor;
    }

    public List<Doctors> getDoctors() {
        return doctors;
    }
    
    public int getCount(){
        return doctors.size();
    }

    @PostConstruct
    public void initDB() {
        emf = PersistenceHolder.getEntityManagerFactory();  
        doctors = new ArrayList<>();
        currentDoctor = new Doctors();
        refreshDoctorList();
    }
    
    public void refreshSpellList(){
        EntityManager em = emf.createEntityManager();
        doctors = em.createQuery("select s from Doctors s")
                .getResultList();
        em.close();
    }

    public void addDoctor() {
//        EntityManager em = emf.createEntityManager();
//
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        try {
//            em.persist(currentDoctor);
//            transaction.commit();
//        } catch (Exception e) {
//            if (transaction.isActive()) {
//                transaction.rollback();
//            }
//            throw e;
//        } finally {
//            em.close();
//        }
//        refreshDoctorList();
//        currentDoctor = new Doctors();
    }
    
    public void onDiffLevelChange() {
        
    }

    private void refreshDoctorList() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
