package com.mycompany.pills_sys.listener;

import com.mycompany.pills_sys.persistence.PersistenceHolder;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        PersistenceHolder.closeEntityManagerFactory();
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        System.out.println("Using JSF: "
                + FacesContext.class.getPackage().getImplementationVersion());
        PersistenceHolder.getEntityManagerFactory();
    }

}
