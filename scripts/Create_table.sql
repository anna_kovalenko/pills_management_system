CREATE TABLE Doctors
(inn NUMBER PRIMARY KEY,
SNP VARCHAR2(30) not null,
job VARCHAR2(25) not null
);

CREATE TABLE Pacients
(inn NUMBER PRIMARY KEY,
SNP VARCHAR2(30) not null,
birthday DATE not null,
phone CHAR (13) not null
);

CREATE TABLE Disease
(id NUMBER PRIMARY KEY,
name VARCHAR2(250) not null,
symptoms VARCHAR2(100) not null
);

CREATE TABLE Pills
(id NUMBER PRIMARY KEY,
name varchar2(80) not null, 
assignment varchar2(200) not null, 
contraindications varchar2(200) not null, 
structure varchar2(200) not null, 
mode_of_app varchar2(200) not null, 
age int);

CREATE TABLE PacientDiagnosis
(id NUMBER PRIMARY KEY,
pacient_inn INT not null,
disease_id INT not null,
CONSTRAINT diagnos_pacient_id_fk FOREIGN KEY (pacient_inn)
   REFERENCES  pacients(inn),
CONSTRAINT diagnos_disease_id_fk FOREIGN KEY (disease_id)
   REFERENCES  disease(id)
);

CREATE TABLE Visit
(
id NUMBER PRIMARY KEY,
pacient_inn INT not null,
doctor_inn INT not null,
date_of_visit DATE not null,
note VARCHAR2(250) not null,
diagnosis VARCHAR2(100) not null,
CONSTRAINT visit_pacient_id_fk FOREIGN KEY (pacient_inn)
   REFERENCES  pacients(inn),
CONSTRAINT visit_doctor_id_fk FOREIGN KEY (doctor_inn)
   REFERENCES  doctors(inn)
);

CREATE TABLE Recipe
(
id NUMBER PRIMARY KEY,
pills_id INT not null,
visit_id INT not null,
CONSTRAINT recipe_pills_id_fk FOREIGN KEY (pills_id)
   REFERENCES  pills(id),
CONSTRAINT visit_recipe_id_fk FOREIGN KEY (visit_id)
   REFERENCES  visit(id)
);