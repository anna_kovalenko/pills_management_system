create sequence my_seq;

--Patients

--begin
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Petrova Z.F.', '09.12.90', '+380636548977');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Sokolnikov G.P.', '10.11.60', '+380965478695');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Sokolnikova T.S.', '23.12.85', '+380965478694');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Lineva N.Z.', '25.02.63', '+380663646566');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Semenov M.N.', '07.01.74', '+380936258588');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Lop H.I.', '30.03.78', '+380984920305');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Aphanasev O.V.', '12.08.99', '+380506472835');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Lisichkina V.E.', '08.09.54', '+380975642312');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Kovalenko A.A.', '09.04.90', '+380671111213');
insert into Pacients (inn, SNP, birthday, phone)
values (my_seq.nextval, 'Pastushkin S.I.', '23.08.48', '+380959991000');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Vinokur K.B.', '11.09.83', '+380985556667');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Zhadan S.A.', '14.02.65', '+380675480000');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Poroshkov K.D.', '14.10.74', '+380666987080');
insert into Pacients (inn, SNP, birthday, phone) 
values (my_seq.nextval, 'Nikolskij F.K.', '25.11.77', '+380638523456');
--end;

--Disease

--begin
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Acute respiratory viral infection', 'runny nose, cough, sneezing, headache');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Intolerance to citrus', 'redness and watering of the eyes, runny nose');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Seasonal rhinitis', 'profuse watery discharge from the nose, sneezing');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Allergic tracheitis', 'whooping cough, scratching in the throat, hoarseness');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Allergic faringopatiya', 'swelling of the mucous membrane of the pharynx');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Allergic laryngitis', 'feeling of awkwardness when swallowing');
insert into Disease (id, name, symptoms) 
values (my_seq.nextval, 'Allergic otitis', 'a large amount of liquid discharge from the external auditory meatus');
--end;

--Pills

--begin
insert into Pills (id, name, assignment, contraindications, structure, mode_of_app, age) 
values (my_seq.nextval, 'Grippostad C', 'Grippostad C for the symptomatic', 'Grippostad C should not be used', '1 capsule formulation Grippostad C contains: Paracetamol - 200 mg', 'Typically, 1-2 capsules', 12);
insert into Pills (id, name, assignment, contraindications, structure, mode_of_app, age) 
values (my_seq.nextval, 'Cetrilev', 'Cetrilev designed to treat pacients', 'Cetrilev contraindicated in pacients', 'Levocetirizine dihydrochloride - 5 mg', 'Cetrilev taken orally', 6);
insert into Pills (id, name, assignment, contraindications, structure, mode_of_app, age) 
values (my_seq.nextval, 'Herbion ivy syrup', 'Herbion ivy syrup used for the treatment', 'Herbion ivy syrup is not indicated for pa�ients', 'dry extract of ivy leaves (5-7.5)', 'Herbion ivy syrup for oral administration', 0.5);
insert into Pills (id, name, assignment, contraindications, structure, mode_of_app, age) 
values (my_seq.nextval, 'Validolum', 'Angina, neurosis, hysteria', 'No', 'A solution of menthol to methyl ester of isovaleric acid', 'Sublingual (under the tongue) 0.06 g or 4-5 drops of the solution', 16);
--end;

--Doctors

--begin
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Petrov U.V.', 'surgeon');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Sidorov U.P.', 'stomatologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Rusanov I.S.', 'cardiologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Bennington C.A.', 'audiologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Ivanova V.P.', 'gynaecologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Solnechnyj M..', 'urologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Pasechnaya E.A.', 'internist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Vasechkina E.A.', 'surgeon');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Kutsenko D.O.', 'stomatologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Manko K.P.', 'cardiologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Kotenko S.V.', 'audiologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Kozyreva V.A.', 'gynaecologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Petrova A.L.', 'urologist');
insert into Doctors (inn, SNP, job) 
values (my_seq.nextval, 'Timochenko D.V.', 'haematologist');
--end;


--Visit

--begin
--insert into Visit (id, doctor_inn, pacient_inn, date_of_visit, note, diagnosis) 
--values (my_seq.nextval, 18, 10, sysdate, 'heart', 'tachycardia');
--insert into Visit (id, doctor_inn, pacient_inn, date_of_visit, note, diagnosis) 
--values (my_seq.nextval, 16, 11, sysdate, 'back', 'hernia');
--insert into Visit (id, doctor_inn, pacient_inn, date_of_visit, note, diagnosis) 
--values (my_seq.nextval, 21, 6, sysdate, 'kidneys', 'kidney stones');
--insert into Visit (id, doctor_inn, pacient_inn, date_of_visit, note, diagnosis) 
--values (my_seq.nextval, 24, 8, sysdate, 'teeth', 'caries');
--insert into Visit (id, doctor_inn, pacient_inn, date_of_visit, note, diagnosis) 
--values (my_seq.nextval, 29, 13, sysdate, 'blood', 'bacillosis');
--end;